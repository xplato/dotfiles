if status is-interactive
    # Commands to run in interactive sessions can go here
end

# VARIABLES

set -g MYVIMRC ~/.config/vim/.vimrc
set -g VIMINIT ":set runtimepath+=~/.config/vim|:source $MYVIMRC"

# FISH CONFIG

set -g fish_color_autosuggestion #fffff### FISH CONFIG

set -g fish_color_autosuggestion #ffffff

# Show nothing, ugh
function fish_greeting
end


# FUNCTIONS

function mkcd
	mkdir $argv && cd $argv
end


# BEGIN ALIASES


## GIT
alias gs='git status'
alias gg='git log --oneline --abbrev-commit --all --graph --decorate --color'

# Add every changed file
alias ga='git add -A .'

# Commit with GPG signing
alias gc='git commit -S'

# gp[space] on fish
abbr -a gp git push origin


## CONFIG
alias conf='vim ~/.config/fish/config.fish'
alias vconf='vim ~/.config/vim/.vimrc'


# The obvious ones
alias c='clear'
alias v='vim'
alias tree='exa -T'
alias exa='exa --icons'

# Mapping ls, etc.
alias ls='exa --icons --group-directories-first'
alias la='exa -a --icons --group-directories-first'
alias lo='exa --icons --group-directories-first --oneline'

# Navigation
alias 1='cd ../'
alias 2='cd ../../'
alias 3='cd ../../../'
alias 4='cd ../../../../'

alias ..='cd ../../'
alias ...='cd ../../../'
alias ....='cd ../../../../'

alias q='cd ../'

# Directories
alias docs='cd ~/Documents'
alias down='cd ~/Downloads'
alias proj='cd ~/Documents/Projects'
alias desk='cd ~/Desktop'

# Yarn
alias dev='yarn dev'
alias start='yarn start'
alias watch='yarn watch'

# Init Starship
starship init fish | source
